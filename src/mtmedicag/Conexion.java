/* 

*MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 * 
 * AUTOR: Leandro Garcia Villamil
 */
package mtmedicag;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import modelo.Empresa;
import modelo.Medico;
import com.opencsv.CSVWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 *
 * @author Leandro Garcia
 */
public class Conexion {
    static java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("mtmedicag/general");
    
    public static Connection  getConnection() 
    {
       Connection  connection = null;
      try{
          connection = DriverManager.getConnection(bundle.getString("urlDB"), bundle.getString("dbUser"), bundle.getString("dbpwd"));
          return connection;          
      }catch(SQLException  e){
          return connection;
      }
    }

    
    public void  truncarServicios()
    { try{
        String loadQuery = bundle.getString("queryTruncar");
        Connection con = getConnection();
        Statement stmt = con.createStatement();
        stmt.execute(loadQuery);
        stmt.close();
        }catch (SQLException   e){ e.printStackTrace();}
    }
    
    public void CargarCSV(String urlfile)
    {
        try{
            //String loadQuery = bundle.getString("queryCargar",urlfile);
        String loadQuery = MessageFormat.format(bundle.getString("queryfinal"),urlfile);
        //System.out.println(loadQuery);
        Connection con = getConnection();
        Statement stmt = con.createStatement();
        stmt.execute(loadQuery);
        stmt.close();
     
        }catch(SQLException   e){ e.printStackTrace();}
    }
    
    public String getRegistrosTabla1()
    {
        try{
            String Query   = bundle.getString("queryRegistros");
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            
            stmt.execute(Query);
            ResultSet  rs = stmt.getResultSet();
            String total = "";
            while(rs.next())
            {
               total = rs.getString("total");
            }
            stmt.close();
            return total;
        }catch(SQLException  e){ e.printStackTrace();  return "nada";}
    }
    
    public String crearEmpresa(String emp)
    {
        
        try{
            String Query = MessageFormat.format(bundle.getString("queryCrearEmp"),emp);
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(Query);
            stmt.close();
            return "1";
        }catch (SQLException  e){e.printStackTrace(); return "0";}
        
    }
    
    public List<Empresa> getEmpresas()
    {
        List<Empresa> list = new ArrayList<Empresa>();
       try{
           String query = bundle.getString("queryGetEmp");
           
           Connection con = getConnection();
           Statement stmt = con.createStatement();
           stmt.execute(query);
           ResultSet  rs = stmt.getResultSet();
            while(rs.next())
            {
                list.add(new Empresa(Integer.parseInt(rs.getString("id")),rs.getString("nombre")));
            }
           
           stmt.close();
           return list;
       }catch (SQLException  e){
           e.printStackTrace();
          return list;
       }
      
    }
    
    public List<Medico> getMedicos(){
        List<Medico> ls = new ArrayList<Medico>();
        try{
            String query = bundle.getString("queryMedicojoin");
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(query);
            ResultSet  rs = stmt.getResultSet();
            while(rs.next())
            {
                ls.add(new Medico((rs.getString("nombre")), rs.getString("pwd"),rs.getString("login")));
            }
            stmt.close();
            return ls;
        }catch(SQLException  e){
           e.printStackTrace();
           return null;
        }
        
    }
    
      public List<Medico> getMedicosAll(){
        List<Medico> ls = new ArrayList<Medico>();
        try{
            String query = bundle.getString("queryGetAllmedicos");
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(query);
            ResultSet  rs = stmt.getResultSet();
            while(rs.next())
            {
                ls.add(new Medico((rs.getString("nombre")), rs.getString("pwd"),rs.getString("login")));
            }
            stmt.close();
            return ls;
        }catch(SQLException  e){
           e.printStackTrace();
           return null;
        }
        
    }
      
    public String borrarRegistro(String login){
      
        try{
            String query =MessageFormat.format(bundle.getString("queryBorrarMedico"),login);
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(query);
            stmt.close();
            return "1";
        }catch(SQLException e){e.printStackTrace(); return "0";}
        
    }
    
    
    public String crearMedico(String nombre,String pwd,Integer emp,String login)
    {
        try{
           String query = MessageFormat.format(bundle.getString("queryCrearMed"),nombre,pwd,emp,login);
           Connection con = getConnection();
           Statement stmt = con.createStatement();
           stmt.execute(query);
           stmt.close();
           return "1";
        }catch(SQLException  e){e.printStackTrace(); return "0";}    
    }
    
    public int checkRegistros()
    {
        try{
            String query = bundle.getString("queryRegistros1");
            Connection  con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(query);
            ResultSet  rs = stmt.getResultSet();
            if(rs.last() == false)
            {
                return 0;
            }else{ return 1;}
        }catch(SQLException e){e.printStackTrace(); return 0; }
    }
    
    public String crearCSV(String nombre,String path) throws IOException
    {
        
    // se obtiene primero la query
     try{
        
         String query = bundle.getString("queryRegistros1");
         Connection  con = getConnection();
         Statement stmt = con.createStatement();
         stmt.execute(query);
         ResultSet  rs = stmt.getResultSet();
         
               
         
         FileOutputStream os = new FileOutputStream(path+nombre+".csv");
         os.write(0xef);
         os.write(0xbb);
         os.write(0xbf);

         CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(os),';');
         
         csvWriter.writeNext(new String[]{
             "ID",
             "COD_RECOBRO",
             "NUM_ITEM",
             "TIPOID",
             "NUMEROID",
             "NOMBREID",
             "NUMERO_FALLO",
             "FECHAFALLO",
             "fec1_instancia",
             "fec2_instancia",
             "coddx",
             "nombredx",
             "aclaraciones_fallo_tutela",
             "tipo_servicio",
             "nom_generico",
             "nom_servicio",
             "cantidad",
             "cada",
             "just_pbsupc",
             "indicaciones",
             "just_medica2",
             "coddx2",
             "codmipres",
             "items",
             "proceso",
             "estado",
             "analista",
             "medico",
             "msg_fallo",
             "tipo_prestacion",
             "frecuencia_u_unidad_tiempo",
             "dura_trata_periodo",
             "tipo_serv_complementario",
             "cant_dur_tratamiento",
             "item_reg",
             "enfermedad_huerfana",
             "cod_enf_unferana",
             "cod_diag_r1",
             "cod_diag_r2",
             "enf_huerfana_principal"
         });
         while(rs.next())
         {
             csvWriter.writeNext(new String[]{
                 rs.getString("id"),
                 rs.getString("COD_RECOBRO"),
                 rs.getString("NUM_ITEM"),
                 rs.getString("TIPOID"),
                 rs.getString("NUMEROID"),
                 rs.getString("NOMBREID"),
                 rs.getString("NUMERO_FALLO"),
                 rs.getString("FECHAFALLO"),
                 rs.getString("fec1_instancia"),
                 rs.getString("fec2_instancia"),
                 rs.getString("coddx"),
                 rs.getString("nombredx"),
                 rs.getString("aclaraciones_fallo_tutela"),
                 rs.getString("tipo_servicio"),
                 rs.getString("nom_generico"),
                 rs.getString("nom_servicio"),
                 rs.getString("cantidad"),
                 rs.getString("cada"),
                 rs.getString("just_pbsupc"),
                 rs.getString("indicaciones"),
                 rs.getString("just_medica2"),
                 rs.getString("coddx2"),
                 rs.getString("codmipres"),
                 rs.getString("items"),
                 rs.getString("proceso"),
                 rs.getString("estado"),
                 rs.getString("analista"),rs.getString("medico"),
                 rs.getString("msg_fallo"),rs.getString("tipo_prestacion"),rs.getString("frecuencia_u_unidad_tiempo"),
                 rs.getString("dura_trata_periodo"),rs.getString("tipo_serv_complementario"),rs.getString("cant_dur_tratamiento"),
                 rs.getString("item_reg"),rs.getString("enfermedad_huerfana"),rs.getString("cod_enf_unferana"),
                 rs.getString("cod_diag_r1"),rs.getString("cod_diag_r2"),
                 rs.getString("enf_huerfana_principal")
                
             });
         }
         csvWriter.close();
         stmt.close();
         
     }catch(SQLException e){e.printStackTrace(); return "0"; }
     return "1";
    }
    
    public static void main(String[] args)
    {
	try
        {   String loadQuery = "Select  * from tabla1";
            Connection con = getConnection();
            Statement stmt = con.createStatement();
            stmt.execute(loadQuery);
            //System.out.println(stmt.getResultSet().);

          stmt.close();
        }
        catch (SQLException  e)
        {
		        e.printStackTrace();
        }
        
        Conexion cone =new  Conexion();
        cone.truncarServicios();
    }   
}