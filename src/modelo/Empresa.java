/* 

*MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 * 
 * AUTOR: Leandro Garcia Villamil
 */
package modelo;

/**
 *
 * @author Usuario
 */
public class Empresa {

    
    Integer id;
    String nombre;

    public Empresa(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
    @Override
  public String toString() {
    return nombre;
  }
   
    
    
    
}
