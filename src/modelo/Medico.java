/* 
*MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 * 
 * AUTOR: Leandro Garcia Villamil
 */
package modelo;

/**
 *
 * @author Usuario
 */
public class Medico {
    
    private String nombre;
    private String password;
    private String login;

    public Medico(String nombre, String password,String login) {
        this.nombre = nombre;
        this.password = password;
        this.login = login;
    }
    
    public String getLogin()
    {
        return login;
    }
    
    public void setLogin(String login){
        this.login = login;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
