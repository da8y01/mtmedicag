/* 

*MYT MEDICA S.A.S Copyright (c) Reservados todos los derechos.
 * 
 * El uso de este Software es de uso exclusivo de MYT MEDICA S.A.S Este
 * Software NO es software libre, no se permite su distribucion, modificacion, compilacion, re-uso o
 * procesos de ingenieria a la inversa sin previa autorizacion de MYT MEDICA S.A.S
 * 
 * MYT MEDICA S.A.S  tiene derecho a realizar cambios al codigo fuente sin
 * previo aviso siendo responsable de dar libre soporte y mantenimiento a este codigo.
 * 
 * AUTOR: Leandro Garcia Villamil
 */
package modelo;

import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class ModeloTabla extends AbstractTableModel {
    
    private String[] columnNames = {"First Name",
                                        "Last Name",
                                        "Sport",
                                        "# of Years",
                                        "Vegetarian"};
    private Object[][] data = {
	    {"Kathy", "Smith",
	     "Snowboarding", new Integer(5), new Boolean(false)},
	    {"John", "Doe",
	     "Rowing", new Integer(3), new Boolean(false)},
	    {"Sue", "Black",
	     "Knitting", new Integer(2), new Boolean(false)},
	    {"Jane", "White",
	     "Speed reading", new Integer(20), new Boolean(false)},
	    {"Joe", "Brown",
	     "Pool", new Integer(10), new Boolean(false)}
        };

    public ModeloTabla(Object[][] data ,String[] columnNames ) {
        this.columnNames = columnNames;
        this.data = data;
    }
        
    public int getColumnCount() {
        return columnNames.length;
    }

    public int getRowCount() {
        return data.length;
    }

     public Object getValueAt(int row, int col) {
        return data[row][col];
    }
     
   public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
   
   public boolean isCellEditable(int row, int col) {
          
            if (col < 3) {
                return false;
            } else {
                return true;
            }
        }
   
   public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
